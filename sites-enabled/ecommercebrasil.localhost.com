
server {


    server_name ecommercebrasil.localhost.com   www.ecommercebrasil.localhost.com;


    access_log off;
    error_log off;


    root /var/www/html/ecommercebrasil;
    
    

    index index.php index.html index.htm;


    include common/php7.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/html/ecommercebrasil/conf/nginx/*.conf;
}
