server {
    listen 80;
    server_name wordpress.localhost.com *.wordpress.localhost.com;

    set $skip_cache 0;

    if ($request_method = POST) {
        set $skip_cache 1;
    }
    if ($query_string != "") {
        set $skip_cache 1;
    }
    
    if ($request_uri = "/wp-admin") {
	return 301 $scheme://$host/wp-admin/;
    }

    if ($request_uri ~* "(/wp-admin/|/xmlrpc.php|/wp-(app|cron|login|register|mail).php|wp-.*.php|/feed/|index.php|wp-comments-popup.php|wp-links-opml.php|wp-locations.php|sitemap(_index)?.xml|[a-z0-9_-]+-sitemap([0-9]+)?.xml)") {
        set $skip_cache 1;
    }
    
    if ($http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass|wordpress_no_cache|wordpress_logged_in") {
        set $skip_cache 1;
    }

    location / {
       set $key "nginx-cache:$scheme$request_method$host$request_uri";
       srcache_fetch_skip $skip_cache;
        srcache_store_skip $skip_cache;

        srcache_response_cache_control off;

        set_escape_uri $escaped_key $key;

        srcache_fetch GET /redis-fetch $key;
        srcache_store PUT /redis-store key=$escaped_key;

        more_set_headers 'X-SRCache-Fetch-Status $srcache_fetch_status';
        more_set_headers 'X-SRCache-Store-Status $srcache_store_status';
        more_set_headers 'L-skip-cache $skip_cache';

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://127.0.0.1:3100;

    }   
    location /redis-fetch {
        internal  ;
        set  $redis_key $args;
        redis_pass  redis;
    }   
    location /redis-store {
        internal  ;
        set_unescape_uri $key $arg_key ;
        redis2_query  set $key $echo_request_body;
        redis2_query expire $key 14400;
        redis2_pass  redis;
    }

}

server {

    listen 3100;
    server_name wordpress.localhost.com *.wordpress.localhost.com;


    access_log off;
    error_log /var/log/nginx/wordpress.log warn;

    root /var/www/html/wp;



    index index.php index.html index.htm;

    include common/pagespeed.conf;
    include common/php.conf;
    include common/wpsubdir.conf;
    include common/wpcommon.conf;
    include common/locations.conf;
}

