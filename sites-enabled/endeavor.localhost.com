server {


    server_name endeavor.localhost.com *.endeavor.localhost.com;


    access_log off;
    error_log /var/log/nginx/endeavor.log warn;

    root /var/www/html/endeavor;
    
    

    index index.php index.html index.htm;

    include common/php.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
}

server {
    listen 443 ssl;
    server_name endeavor.localhost.com *.endeavor.localhost.com;
    ssl_certificate /etc/nginx/ssl/00-default-nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/00-default-nginx.key;


     access_log off;
    error_log /var/log/nginx/endeavor.log warn;

    root /var/www/html/endeavor;



    index index.php index.html index.htm;

    include common/php.conf;
    include common/wpcommon.conf;
    include common/locations.conf;

}

