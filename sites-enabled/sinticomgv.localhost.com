server {


    server_name sinticomgv.localhost.com *.sinticomgv.localhost.com;


    access_log off;
    error_log /var/log/nginx/sinticomgv.log warn;

    root /var/www/html/sinticomgv.com.br;
    
    

    index index.php index.html index.htm;

    include common/php.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
}
