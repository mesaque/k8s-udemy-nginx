server {


    server_name herbalforce.localhost.com herbalforce.com.br;


    access_log off;

    root /var/www/html/herbalforce.com.br;
    
    

    index index.php index.html index.htm;

    include common/php.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
}
