
server {


    server_name personare.localhost.com   www.personare.localhost.com;


    access_log off;
    error_log off;


    root /var/www/html/personare;
    
    

    index index.php index.html index.htm;


    include common/php7.conf;      
    include common/wpcommon.conf;
    include common/locations.conf;
    include /var/www/html/personare/conf/nginx/*.conf;
}
