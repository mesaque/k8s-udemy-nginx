server {


    server_name magento.localhost.com;


    access_log off;

    set $MAGE_ROOT /var/www/html/magento;
    include /var/www/html/magento/nginx.conf.sample;
    

    index index.php index.html index.htm;

    include common/wpcommon.conf;
    include common/locations.conf;
}
